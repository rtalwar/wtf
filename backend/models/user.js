define(["dojo/_base/declare","dojo/node!mongoose","backend/schemas/user"],
  function(declare,mongoose, UserSchema ) {


   return mongoose.model('users', UserSchema);

  });