define(["dojo/_base/declare","dojo/node!mongoose","backend/schemas/addressBook"],
  function(declare,mongoose, AddressBookSchema ) {

   return mongoose.model('addressBooks', AddressBookSchema);

  });