define(["dojo/_base/declare","dojo/node!mongoose","backend/schemas/notification"],
  function(declare,mongoose, NotificationSchema ) {
   return mongoose.model('notification_dump', NotificationSchema);

  });