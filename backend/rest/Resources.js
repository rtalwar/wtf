
require(["dojo/node!fs","dojo/_base/array"],function(fs, array){
    //scan all the files in <restResourceRoot> directory and load them via require
    var fileArray = [],
        restResourceRoot = "backend/rest/",
        excludeFiles = ["Resources.js"];

    fs.readdir(require.toUrl(restResourceRoot), function(err,files){
        if(!err) {
            array.forEach(files, function (file) {
                if(excludeFiles.indexOf(file) === -1) {
                    fileArray.push(restResourceRoot + file);
                }
            });
            require(fileArray,function(){
                console.log("Loaded RESTful resources - ");
                console.log(fileArray);

            });
        }
    });
});
