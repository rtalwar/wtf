require([
    "backend/global",
    "backend/models/user",
    "backend/lib/AppModel"

], function (global, Model, AppModel) {

    var path = "/users";
    var app = global.app;
    var appModel = new AppModel({model:Model});


    app.get(path, function (req, res, next) {
        return appModel.find(req,res,next);
    });

    app.get(path + "/:id", function (req, res, next) {
        return appModel.findOne(req,res,next);
    });

    app.put(path + "/:id", function (req, res, next) {
        return appModel.findByIdAndUpdate(req,res,next);
    });
    app.post(path, function (req, res, next) {

        return appModel.add(req,res,next);
    });
    app.delete(path + "/:id", function (req, res, next) {
        return appModel.findByIdAndRemove(req,res,next);
    });



});
