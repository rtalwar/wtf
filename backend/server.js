require(["dojo/node!express",
        "dojo/node!http",
        "dojo/node!socket.io",
        "backend/global",
        "backend/lib/db-config",
        "backend/config/config",
        "backend/lib/SocketServer",
        "backend/lib/PushNotificationServer",
        "dojo/node!apn",
        "backend/models/user"
    ],
    function (
        express,
        http,
        socketio,
        global,
        dbConfig,
        appConfig,
        SocketServer,
        PushNotificationServer,
        apn,
        UserModel
        ) {

        dbConfig.connectDatabase(function (db){

            var app = express(),server = http.createServer(app),io = socketio.listen(server),

            allowCrossDomain = function(req, res, next) {
                res.header('Access-Control-Allow-Origin', '*');
                res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
                res.header('Access-Control-Allow-Headers', 'Content-Type,Authorization');

                // intercept OPTIONS method
                if ('OPTIONS' === req.method) {
                    res.send(200);
                }
                else {
                    next();
                }
            },
             options = {
                key: appConfig.get("apn:key"),
                cert: appConfig.get("apn:cert"),
                passphrase: appConfig.get("apn:passphrase")
             }, server;

            /**
             * Express Configuration
             */
            app.configure(function() {
                app.use(express.logger('dev'));
                app.use(express.compress());
                app.use(express.methodOverride());
                app.use(express.cookieParser());
                app.use(express.bodyParser());
                app.use(allowCrossDomain);
                app.use(express.static(require.toUrl("shared")));
                app.use(express.static(require.toUrl("frontend")));
                app.use(express.static(require.toUrl("js-lib")));
            });

            app.configure('development', function() {
                app.use(express.errorHandler({
                    dumpExceptions: true,
                    showStack: true
                }));
            });

            /*this sets up errorhandler for all the middlewares before app.router so
             * that if any of the middlewares above throw an error,they are catched by
             * express.errorHandler*/
            app.configure('development', function () {
                app.use(express.errorHandler({
                    dumpExceptions: true,
                    showStack: true
                }));
            });

            app.configure('production', function () {
                app.use(express.errorHandler());
            });

            app.use(app.router);

            // Setup Express error handler middleware for requests and routes
            app.use(function (err, req, res, next) {
                res.send(err.code, {error: err});
            });

            if(db){
                server.listen(appConfig.get("main:port"));
                console.log('listening on port ',appConfig.get("main:port"));
                SocketServer.startServer();
            }
            global.app = app;
            global.server = server;
            global.io = io;



            server = new apn.Connection(options);

            global.pushNotificationServer = new PushNotificationServer(
                {
                    server:server,
                    gcmKey:appConfig.get("gcm:key"),
                    userModel:UserModel,
                    apn:apn
                }
            );
            require(["backend/rest/Resources"],function(){
            });

        });



    });