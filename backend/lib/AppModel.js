define(["dojo/_base/declare",
        "backend/lib/custom-errors"
    ],
    function(
        declare,
        customError
        ) {

        return declare("backend.lib.AppModel", [], {

            constructor: function(args) {
                if(!args.model){
                    throw new customError.InternalServer('Database model not found');
                }
               this.model = args.model;
            },
            findOne:function(req,res,next){
              this.model.findOne({'_id':req.params.id},function(err,result){
                    return err ? next(new customError.Database(err.toString())) : res.send(result?200:404,result);
                }
            );
            },
            find:function(req,res,next){
                this.model.find(req.query,function(err,result){
                    return err ? next(new customError.Database(err.toString())) : res.send(result?200:404,result);
                });
            },
            add:function(req,res,next){
                new this.model(req.body).save(function(err,result){
                    return err ? next(new customError.Database(err.toString())) : res.send(200,result);

                });
            },
            findByIdAndUpdate:function(req,res,next){
                this.model.findByIdAndUpdate(req.params.id, req.body, function(err, result){
                    return err ? next(new customError.Database(err.toString())) : res.send(result?200:404,result);
                });
            },
            findByIdAndRemove:function(req,res,next){

                this.model.findByIdAndRemove(req.params.id, function(err, result){
                    return err ? next(new customError.Database(err.toString())) : res.send(result?200:404,result);
                });
            }

        });
    });