define([
    "dojo/_base/declare",
    "dojo/_base/lang",
    "rijit/helpers/Log",
    "dojo/on",
    "dojo/request"
], function (declare, lang, Log, on, request) {

    return declare("", [], {
        server: null,
        gcmKey:null,
        constructor: function (options) {
            this._notificationSender = {
                "0": lang.hitch(this, this._sendAndroidNotification),
                "1": lang.hitch(this, this._sendIOSNotification)
            };
            for(var i in options){
                this[i] = options[i];
            }

            this._configureServer();

        },
        _getNotificationSender: function (deviceOs) {
            var sender = this._notificationSender[deviceOs];
            return sender || function () {
                Log.error('NotificationSender not found');
            };
        },
        _socketError: function (error) {
            Log.error(error.message);
            this._configureServer();
        },
        _configureServer: function () {
            if(this.server) {
                on(this.server, 'socketError', lang.hitch(this, this._socketError));
            }
        },

        send: function (user_id, notification) {
            var self = this;


//      self._sendIOSNotification("f225d9d72c79463705c8b2e16f24c8cf0583e6dd0b09e29cb315f277bd055c3e",notification);
            this.userModel.findById(user_id, function (err, user) {
                var deviceid = user && user.device && user.device.device_id,
                    deviceOs = user && user.device && user.device.OS && String(user.device.OS);
                self.sendNotification(deviceOs, deviceid, notification);

            });

        },
        sendNotification: function (deviceOs, deviceid, notification) {
            (this._getNotificationSender(deviceOs))(deviceid, notification);
        },
        _getGCMRequestOptions: function (device_id, notificationObject) {
            return  {
                handleAs: "json",
                headers: {
                    'Authorization': "key=" + this.gcmKey,
                    'Content-Type': "application/json"
                },
                method: "POST",
                data: JSON.stringify(this._getGCMRequestData(device_id, notificationObject))
            };
        },

        _getGCMRequestData: function (device_id, notificationObject) {
            return {
                registration_ids: [device_id],
                data: {
                    type: notificationObject.type,
                    user_name: notificationObject.user_name || "",
                    alert: notificationObject.data,
                    payload: notificationObject
                }
            };
        },
        _makeGCMRequest: function (options) {
            var self = this;
            request("https://android.googleapis.com/gcm/send", options).then(function (data) {
                self._successfullGCMRequest(true);
            }, function (error) {
                self._successfullGCMRequest(false);
            });
        },
        _successfullGCMRequest:function(status){
            Log.info("Android notification sent successfully");

        },
        _sendAndroidNotification: function (device_id, notificationObject) {
            var options = this._getGCMRequestOptions(device_id, notificationObject);
            this._makeGCMRequest(options);
        },

        _getApnNotificationObject: function (notificationObject) {
            var notification = new this.apn.Notification();
            notification.alert = notificationObject.data;
            notification.payload = notificationObject.payload || {type: notificationObject.type};
            notification.sound = 'default';
            notification.badge = 0;
            return notification;
        },

        _getApnDeviceObjectFromDeviceId: function (device_id) {
            var device = new this.apn.Device(device_id);
            return device;
        },

        _sendIOSNotification: function (device_id, notificationObject) {
            try {
                var device = this._getApnDeviceObjectFromDeviceId(device_id);
                var notification = this._getApnNotificationObject(notificationObject);
                this.server.pushNotification(notification, device);
            } catch (e) {
                Log.error('Not able to send push notification' + e.message);
            }

        }
    });

});