define([
    "dojo/_base/declare",
    "backend/global",
    "dojo/node!net",
    "rijit/helpers/Log",
    "dojo/_base/lang"
], function(declare,global,net,Log,lang){

    /*
     events.js:72
     throw er; // Unhandled 'error' event
     ^
     Error: read ECONNRESET
     at errnoException (net.js:904:11)
     at TCP.onread (net.js:558:19)
     */var SocketServer  = declare("", [], {
        server:null,

        startServer:function(){
            this._destroySockets();
            this.sockets = [];
            this.socketMap = {};
            this.unVerifiedSockets = [];
            this.server = net.createServer();
            this.server.on('connection',lang.hitch(this,this._newConnections));
            this.server.on('error',lang.hitch(this,this._serverError));
            this.server.on('close',lang.hitch(this,this._serverClosed));
            this.server.listen(4001);
        },

        _destroySockets: function (){
            if(this.sockets instanceof Array){
                this.sockets.forEach(function(socket){
                    if(typeof socket.destroy === 'function'){
                        socket.end();
                        socket.destroy();
                    }
                });
            }
        },

        _serverError:function(err){
            Log.error('Server error:', err.message);

        },

        _serverClosed:function(){
            Log.error('Server closed');

        },

        badData :function(socket,sockerArray,error,requestData,close){
            Log.error(error);
            var obj = {code:400,data:error},index;
            if(close){
                Log.error('bad socket data,closing socket');
                socket.end(JSON.stringify(obj));
                socket.destroy();
                index = sockerArray.indexOf(socket);
                this.sockets.splice(index, 1);
            }else{
                try{
                    socket.write(JSON.stringify(obj));
                }catch(e){
                    Log.error('Could not write to socket - '+e.message);
                }

            }

        },
        _newConnections:function(socket){
            this.unVerifiedSockets.push(socket);
            socket.on('data',lang.hitch(this,this._gotSocketData,socket));
            socket.on('close',lang.hitch(this,this._socketClosed,socket));
            socket.on('end',lang.hitch(this,this._socketEnded,socket));
            socket.on('error',lang.hitch(this,this._socketError,socket));
        },

        _socketClosed:function(socket){
            Log.info('connection closed');
            this._endSocketGracefully(socket);
        },
        _socketEnded:function(socket){
            Log.info('socket ended');
            this._endSocketGracefully(socket);
        },
        _socketError:function(socket,error){
            Log.info('socket error-'+error.message);
            this._endSocketGracefully(socket);
            socket.destroy();
        },
        _endSocketGracefully:function(socket){
            socket.end();
            var index = this.sockets.indexOf(socket);
            if(index>=0) {
                this.sockets.splice(index, 1);
            }
        },
        sendSocketNotificationToUserId:function(user_id, notification){

            var receipentSocket = this.socketMap[String(user_id)],
                index = this.sockets.indexOf(receipentSocket);
            if(index>=0) {
                receipentSocket.write(JSON.stringify(notification));
            } else {
                this.sendPushNotificationToUserId(user_id, notification);
            }
        },
        sendPushNotificationToUserId: function(user_id, notification){
            global.pushNotificationServer.send(user_id,notification);
        },
        sendPushNotificationToDevice: function(deviceOs,deviceId,notification){
            global.pushNotificationServer.sendNotification(deviceOs,deviceId,notification);
        },
        _gotSocketData:function(socket, data){

//    console.log('got data:', data.toString());
            var index = this.unVerifiedSockets.indexOf(socket),
                obj = null;

            if(index === -1){ // not in unVerifiedSockets,and has been moved to sockets
                //send message to recepient

                try {

                    obj = JSON.parse(data.toString().trim());

                } catch(e) {

                    this.badData(socket,this.sockets,e.toString(),data);
                    return;

                }

                if(obj.to_user_id){
                    this.sendSocketNotificationToUserId(obj.to_user_id,obj);
                }

            } else {
                //verify and move to sockets array

                obj = null;

                try {

                    obj = JSON.parse(data.toString());

                } catch(e) {

                    this.badData(socket,this.unVerifiedSockets,e.toString(),data);
                    return;

                }
                //search for user_id in the data
                if(obj && obj.user_id){

                    Log.info('moved socket to verified sockets, user_id:'+String(obj.user_id));
                    this.socketMap[String(obj.user_id)] = socket;
                    this.sockets.push(socket);
                    this.unVerifiedSockets.splice(index,1);
                    try{
                        socket.write(JSON.stringify({code:200,disableAndroidChat:1,data:"Logged in successfully"}));
                    }catch(e){
                        Log.error('Could not write to socket - '+e.message);
                    }
                } else {

                    this.badData(this.socket,this.unVerifiedSockets,'No user id found in the data',data);

                }

            }

        }
    });

    return new SocketServer();

});