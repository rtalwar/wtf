define(["dojo/_base/declare","rijit/helpers/Log","backend/config/config","dojo/node!mongoose"],
  function(declare,Log,config,mongoose ) {

    var dbConfig = {};

    dbConfig.connectDatabase = function(callback){
        mongoose.connect(config.get("db:mongodb:url"),config.get("db:mongodb:dbOptions"));
        var conn = mongoose.connection;

        //console.log(conn);
        conn.on('error', function(err){
            console.log(err);
            console.log('Error.. connecting to database');
            process.exit(1);
        });

        conn.once('open', function() {
            callback(conn);
        });
    };
    return dbConfig;

  });