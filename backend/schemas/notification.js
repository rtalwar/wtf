define(["dojo/node!mongoose"],
  function(mongoose ) {


    var   Schema = mongoose.Schema;

/*Sample data
{ "user_id" : ObjectId("531abd4211ce9386cd1ca878"),
"_id" : ObjectId("53816428dad140da353e34a8"),
 "notifications" :
          [   {
            "type" : "chat",
            "data" : "yo2",
            "to_user_id" : "2",
            "user_id" : "1"
            }
         ],
  "__v" : 0
}
*/
   var NotificaitonSchema = new Schema({
       "notifications": [{}],
       "user_id":{type:Schema.Types.ObjectId,index:true}
   });

   return NotificaitonSchema;

  });