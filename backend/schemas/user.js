

define(["dojo/node!mongoose"],
  function(mongoose ) {


    var   Schema = mongoose.Schema;

   // var ChatSchema = new Schema({
   //     "user_id": String,
   //     "to_user_id": String,
   //     "data": String,
   //     "status": String,
   //     timestamp: {type: Date, default: Date.now}
   // });

var UserSchema = new Schema({

    "name": String,
    "phone": {type:String, index: { unique: true }},
    "addressBook": {type: Schema.Types.ObjectId, ref:'addressBooks'},
    "device": [{
        OS: Number,
        device_id: String,
        device_name: String
    }]
});

   return UserSchema;

  });