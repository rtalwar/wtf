define(["rijit/helpers/Log","dojo/node!nconf","dojo/node!path"],
  function(Log,nconf,path ) {

     //
     // Setup nconf to use (in-order):
     //   1. Command-line arguments
     //   2. Environment variables
     //   3. A file located at 'path/to/config.json'
     //

     //defaulting to production, use NODE_ENV=development notifications.js for debug purpose
     var currentEnv = process.env.NODE_ENV || "production";
     Log.info(path.join('backend/config/',currentEnv+'.json'));
     nconf.argv()
         .env()
         .file({ file: path.join('backend/config/',currentEnv+'.json') });

     return nconf;

  });