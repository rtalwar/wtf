define(["dojo/_base/declare",
        "dojo/_base/array"
    ],
    function(
        declare,
        array
        ) {

        return declare("", [], {
            constructor:function(args){
              this._store = args.store;
            },
            findOne:function(obj,callBack){
                if(obj && obj._id){
                    array.forEach(this._store,function(doc){
                        if(doc._id === obj._id){
                            callBack(null,doc);
                            return;
                        }
                    });
                }
                callBack(new Error('NotFound'),undefined);
            },
            find:function(obj,callBack){
                var returnedItems = [];
                if(obj ){
                    array.forEach(this._store,function(doc){
                        var flag = true;
                        for(var i in obj){
                            if(obj[i] !== doc[i]){
                                flag = false;
                                break;
                            }
                        }
                        if(flag){
                            returnedItems.push(doc);
                        }


                    });
                    callBack(null,returnedItems);
                    return;
                }
                callBack(new Error('NotFound'),undefined);
            },
            save:function(callBack){

                callBack(null,{_id:"6",name:"amrita"});
            }

        });
    });