// Learn more about configuring this file at <https://github.com/theintern/intern/wiki/Configuring-Intern>.
// These default settings work OK for most people. The options that *must* be changed below are the
// packages, suites, excludeInstrumentation, and (if you want functional tests) functionalSuites.var dojoConfig = {
//http://localhost:63342/dojoexpress/node_modules/intern/client.html?config=tests/intern
var dojoConfig = {
    requestProvider: 'dojo/request/registry'
};
define(['intern/node_modules/dojo/has'],function(has) {
    has.add('dojo-has-api', true);

    return {
        // The port on which the instrumenting proxy will listen
        proxyPort: 9000,

        // A fully qualified URL to the Intern proxy
        proxyUrl: 'http://localhost:9000/',

        // Default desired capabilities for all environments. Individual capabilities can be overridden by any of the
        // specified browser environments in the `environments` array below as well. See
        // https://code.google.com/p/selenium/wiki/DesiredCapabilities for standard Selenium capabilities and
        // https://saucelabs.com/docs/additional-config#desired-capabilities for Sauce Labs capabilities.
        // Note that the `build` capability will be filled in with the current commit ID from the Travis CI environment
        // automatically
        capabilities: {
            'selenium-version': '2.41.0'
        },
        hasCache: {
            "host-node": 1, // Ensure we "force" the loader into Node.js mode
            "dom": 0 // Ensure that none of the code assumes we have a DOM
        },
        // Browsers to run integration testing against. Note that version numbers must be strings if used with Sauce
        // OnDemand. Options that will be permutated are browserName, version, platform, and platformVersion; any other
        // capabilities options specified for an environment will be copied as-is
        environments: [
            { browserName: 'chrome' }

        ],

        // Maximum number of simultaneous integration tests that should be executed on the remote WebDriver service
        maxConcurrency: 3,

        // Name of the tunnel class to use for WebDriver tests
//    tunnel: 'SauceLabsTunnel',

        // The desired AMD loader to use when running unit tests (client.html/client.js). Omit to use the default Dojo
        // loader

        // Configuration options for the module loader; any AMD configuration options supported by the specified AMD loader
        // can be used here
        useLoader: {
            'host-node': 'dojo/dojo',
            'host-browser': 'node_modules/dojo/dojo.js'
        },
        loader: {
            // Packages that should be registered with the loader in each testing environment
            packages: [
                {
                    name: "rijit",
                    location: "shared/rijit"
                },
                {
                    name: "shared",
                    location: "shared"
                },
                {
                    name: "frontend",
                    location: "frontend"
                },
                {
                    name: "backend",
                    location: "backend"
                },
                {
                    name: "dojo",
                    location: "bower_components/dojo"
                }

            ]
        },

        // Non-functional test suite(s) to run in each browser
        suites: [  'tests/lib/PushNotificationServerTest.js','tests/lib/AppModelTest.js'],

        // Functional test suite(s) to run in each browser once non-functional tests are completed
        functionalSuites: [ /* 'myPackage/tests/functional' */ ],

        // A regular expression matching URLs to files that should not be included in code coverage analysis
        excludeInstrumentation: /^tests|js\/lib\/*|node_modules\/|bower_components\//
    };

});
