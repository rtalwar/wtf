define([
    'intern!object',
    'intern/chai!assert',
    'dojo/aspect',
    //    'backend/lib/PushNotificationServer',
    '../support/amdMocker'
], function (registerSuite, assert, aspect/*,PushNotificationServer*/, amdMocker) {
    var MockedNotificationServer;
    var android_os = 0, iphone_os = 1, gcmKey = 'fake_gcm_key';
    var mockNotification = function () {

    };
    var mockDevice = function(device_id){
        this.device_id = device_id;
    };
    var getMockAPN = function(){
        return {
            Notification: mockNotification,
            Device:mockDevice
        };
    };
    var getMockAPNServer = function(){
        return {
            pushNotification:function(){}
        };
    };


    function getMockNotificationServerInstance() {
        var pushServer = new MockedNotificationServer({
            apn: getMockAPN(),
            server: getMockAPNServer(),
            gcmKey: gcmKey
        });
        return pushServer;
    }

    function getMockNotificationObject() {
        var notificationObject = {
            'data': "Hello, what are you doing",
            'type': 'chat'
        };
        return notificationObject;
    }

    registerSuite({
        name: 'PushNotificationServer',
        setup: function () {
//
            return amdMocker.mock('backend/lib/PushNotificationServer', {
                'dojo/request': 'tests/mocks/request'
            }).then(function (mocked) {
                MockedNotificationServer = mocked;
                MockedNotificationServer.prototype._configureServer = function(){};
            });
//            requestMocker.start();

        },
        teardown: function () {
//            requestMocker.stop();

        },
        _getApnNotificationObjectTest: function () {

            var pushServer = getMockNotificationServerInstance();
            var notificationObject = getMockNotificationObject();

              var  apnNotificationObject = pushServer._getApnNotificationObject(notificationObject);
            assert.strictEqual(apnNotificationObject.alert, "Hello, what are you doing",
                'PushNotificationServer._getApnNotificationObject should copy the data field of input NotificationObject to alert field of APN object');
        },
        sendAndroidNotificationTest: function () {

            var dfd = this.async(1000);
            var pushServer = getMockNotificationServerInstance();
            var notificationObject = getMockNotificationObject();

            var expectedOptions = { handleAs: 'json',
                headers: { Authorization: 'key=fake_gcm_key',
                    'Content-Type': 'application/json' },
                method: 'POST',
                data: '{"registration_ids":["fake_id"],"data":{"type":"chat","user_name":"","alert":"Hello, what are you doing","payload":{"data":"Hello, what are you doing","type":"chat"}}}' };
            //spy _makeGCMRequest function

            pushServer._successfullGCMRequest = dfd.callback(function (status) {
                assert.isTrue(status, 'GCM request sent successfully');
            });
            //spy _makeGCMRequest function

            aspect.before(pushServer, "_makeGCMRequest", function (options) {
                assert.deepEqual(expectedOptions, options, "Correct options should be passed to _makeGCMRequest");
                return [options];
            });
            pushServer.sendNotification(android_os, "fake_id", notificationObject);

        },
        sendIosNotificationTest: function () {
            var dfd = this.async(1000);

            var pushServer = getMockNotificationServerInstance();
            var notificationObject = getMockNotificationObject();

            aspect.before(pushServer.server, "pushNotification", dfd.callback(function (notification,device) {
                assert.strictEqual(notification.alert,notificationObject.data,"Correct alert should be set for APN Notification object");
                assert.deepEqual(notification.payload,{type:notificationObject.type},"Correct payload should be set for APN Notification object");

                assert.strictEqual(notification.badge,0, "Badge set for the Apn Notification object should be 0");
                assert.strictEqual(device.device_id,"fake_id", "Correct device_id set for APN device object");

                return [notification,device];
            }));
            pushServer.sendNotification(iphone_os, "fake_id", notificationObject);

        }
    });
});