define([
    'intern!object',
    'intern/chai!assert',
    'dojo/node!fs',
    '../support/amdMocker',
    '../support/DummyModel'
], function (registerSuite, assert, fs, amdMocker, DummyModel) {
    var MockedModule;
    var dummyStore = [];

  var data  =   fs.readFileSync("tests/lib/data/AppModelTest.json");
   dummyStore = JSON.parse(data);



    registerSuite({
        name: 'AppModelTest',
        setup: function () {
            return amdMocker.mock('backend/lib/AppModel', {
//                'dojo/request': 'tests/mocks/request'
            }).then(function (mocked) {
                MockedModule = mocked;
            });

        },
        teardown: function () {
            console.log('teardown');
        },
        appModelFindOneTest: function () {
            var dummyModel = new DummyModel({store: dummyStore});
            var req =  {
                    params: {
                        'id': dummyStore[0]._id
                    }
            };
            var res = {};

            res.send = function (status, data) {

                assert.deepEqual(data, dummyStore[0]);
            };
            var appModel = new MockedModule({model: dummyModel});

            appModel.findOne(req, res, function(){
                console.log('called next');
            });
        },
        appModelFindAllTest: function () {
            var dummyModel = new DummyModel({store: dummyStore});
            var req = {query:{}};
            var res = {};

            res.send =  function (status, data) {
                assert.deepEqual(data, dummyStore);
            };
            var appModel = new MockedModule({model: dummyModel});

            appModel.find(req, res, function(){
                console.log('called next');
            });
        },
        appModelAddTest: function () {
            var req = {body:{name:'amrita'}};
            var res = {};

            res.send =  function (status, data) {
                assert.deepEqual(data.name, "amrita");
            };
            var appModel = new MockedModule({model: DummyModel});
            appModel.add(req, res, function(){
                console.log('called next');
            });
        }
    });
});