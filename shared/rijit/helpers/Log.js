define(["dojo/_base/declare"], function(declare) {

    var Log  = declare("rijit.helpers.Log", [], {

        error: function(error) {
          console.error(error);
          // alert(error);
        },
        info: function(mesg){
          console.log(mesg);
        },
        debug: function(mesg){
            console.log(mesg);
        }

    });
   return (new Log());

  });