define(["dojo/_base/xhr", "dojo/_base/lang", "dojo/json", "dojo/_base/declare", "dojo/store/util/QueryResults","dojo/Deferred","../helpers/Log" /*=====, "./api/Store" =====*/
], function(xhr, lang, JSON, declare, QueryResults, Deferred, Log /*=====, Store =====*/){

// No base class, but for purposes of documentation, the base class is dojo/store/api/Store
var base = null;
/*===== base = Store; =====*/

/*=====
var __HeaderOptions = {
		// headers: Object?
		//		Additional headers to send along with the request.
	},
	__PutDirectives = declare(Store.PutDirectives, __HeaderOptions),
	__QueryOptions = declare(Store.QueryOptions, __HeaderOptions);
=====*/

return declare("shared.store.PlacesStore", base, {
	// summary:
	//		This is a basic store for RESTful communicating with a server through JSON
	//		formatted data. It implements dojo/store/api/Store.

	constructor: function(options){
		// summary:
		//		This is a basic store for RESTful communicating with a server through JSON
		//		formatted data.
		// options: dojo/store/JsonRest
		//		This provides any configuration information that will be mixed into the store
		this.headers = {};
		declare.safeMixin(this, options);



	},

	// headers: Object
	//		Additional headers to pass in all requests to the server. These can be overridden
	//		by passing additional headers to calls to the store.
	headers: {},
	autoIncrement : false,
	// target: String
	//		The target base URL to use for all requests to the server. This string will be
	//		prepended to the id to generate the URL (relative or absolute) for requests
	//		sent to the server
	target: "",

	// idProperty: String
	//		Indicates the property to use as the identity property. The values of this
	//		property should be unique.
	idProperty: "id",


	// sortParam: String
	//		The query parameter to used for holding sort information. If this is omitted, than
	//		the sort information is included in a functional query token to avoid colliding
	//		with the set of name/value pairs.

	// ascendingPrefix: String
	//		The prefix to apply to sort attribute names that are ascending
	ascendingPrefix: "+",

	// descendingPrefix: String
	//		The prefix to apply to sort attribute names that are ascending
	descendingPrefix: "-",



	get: function(id, options){


		var request = {
		  reference: id
		};
		var def = new Deferred();

		function callback(place, status) {
		  if (status == google.maps.places.PlacesServiceStatus.OK) {
		    def.resolve(place);
		  }
		}


		service = new google.maps.places.PlacesService(map);
		service.getDetails(request, callback);

		return def.promise;




	},

	// accepts: String
	//		Defines the Accept header to use on HTTP requests
	accepts: "application/javascript, application/json",

	getIdentity: function(object){
		// summary:
		//		Returns an object's identity
		// object: Object
		//		The object to get the identity from
		// returns: Number
		return object[this.idProperty];
	},

	put: function(data, options){
		// summary:
		//		Stores an object. This will trigger a PUT request to the server
		//		if the object has an id, otherwise it will trigger a POST request.
		// object: Object
		//		The object to store.
		// options: __PutDirectives?
		//		Additional metadata for storing the data.  Includes an "id"
		//		property if a specific id is to be used.
		// returns: dojo/_base/Deferred
		// options = options || {};
		// var id = ("id" in options) ? options.id : this.getIdentity(object);
		// var hasId = typeof id != "undefined";
		// return xhr(hasId && !options.incremental ? "PUT" : "POST", {
		// 		url: hasId ? this.target + id : this.target,
		// 		postData: JSON.stringify(object),
		// 		handleAs: "json",
		// 		headers: lang.mixin({
		// 			"Content-Type": "application/json",
		// 			Accept: this.accepts,
		// 			"If-Match": options.overwrite === true ? "*" : null,
		// 			"If-None-Match": options.overwrite === false ? "*" : null
		// 		}, this.headers, options.headers)
		// 	});

		var def = new Deferred();
		data  = data || {};
		if(typeof data[this.idProperty] === "undefined" || data[this.idProperty] === null){
		  data[this.idProperty] = null;
		}

          var objectStore  = this._getObjectStore("readwrite");
          if(objectStore.autoIncrement){
              data[this.idProperty] = parseInt(data[this.idProperty]);

          }

      var request = objectStore.get(data[this.idProperty]);
      var self = this;
       request.onerror = function(event){
         def.reject(event.target.error.message);
        };
      request.onsuccess = function(event){
        var newData  = request.result;
        lang.mixin(newData,data);
        var requestUpdate = null;
        if(self.autoIncrement){
        	 requestUpdate = objectStore.put(newData,newData[self.idProperty]);
    	} else{
    		 requestUpdate = objectStore.put(newData);
    	}
        requestUpdate.onerror = function(event){
         def.reject(event.target.error.message);
        };
        requestUpdate.onsuccess = function(event){
          def.resolve(event.target.result);
          self.notify(newData,event.target.result);
        };

      };
      return def.promise;

	},

	add: function(data, options){
		// summary:
		//		Adds an object. This will trigger a PUT request to the server
		//		if the object has an id, otherwise it will trigger a POST request.
		// object: Object
		//		The object to store.
		// options: __PutDirectives?
		//		Additional metadata for storing the data.  Includes an "id"
		//		property if a specific id is to be used.
		// options = options || {};
		// options.overwrite = false;
		// return this.put(object, options);

		if(typeof options[this.idProperty] !== "undefined" && options[this.idProperty] !== null){
			return this.put(data,options);
		}

		var def = new Deferred();
		var self = this;
		var request = this._getObjectStore("readwrite").add(data);
		request.onerror = function(event){
		  def.reject(event.target.error.message);
		};
		request.onsuccess = function(event){
			var result = {};
			result[self.idProperty]=event.target.result;
		   def.resolve(lang.mixin(data,result));
		   // self.notify(lang.mixin(data,{id:event.target.result}));
		};
		//  alert("Name for SSN 444-44-4444 is " + request.result.name);
		return def.promise;
	},

	remove: function(id, options){
		// summary:
		//		Deletes an object by its identity. This will trigger a DELETE request to the server.
		// id: Number
		//		The identity to use to delete the object
		// options: __HeaderOptions?
		//		HTTP headers.
		// options = options || {};
		// return xhr("DELETE", {
		// 	url: this.target + id,
		// 	headers: lang.mixin({}, this.headers, options.headers)
		// });

		var def = new Deferred();
		var self = this;

		    var objectStore  = this._getObjectStore("readwrite");
		    if(objectStore.autoIncrement){
		        id = parseInt(id);

		    }
		    var request = objectStore['delete'](id);

		    request.onerror = function(event){
		      def.reject(event.target.error.message);
		    };

		    request.onsuccess = function(event){
		      def.resolve(event.target.result);
		      self.notify(undefined,event.target.result);

		    };
		    return def.promise;
	},

	query: function(query, options){

		var def = new Deferred();
		var promiseResults =  QueryResults(def.promise);

				  var service = new google.maps.places.AutocompleteService();

				  var searchRegexStr = query['description'].toString();
				  var searchText = searchRegexStr.slice(0,searchRegexStr.length-1);

		  service.getQueryPredictions({ input: searchText }, callback);

		 function callback(predictions, status) {
			  if (status != google.maps.places.PlacesServiceStatus.OK) {
			    alert(status);
			    return;
			  }
			  promiseResults.total = predictions.length;

			  def.resolve(predictions);
		}

		return promiseResults;


	}
});

});