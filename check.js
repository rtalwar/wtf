//var first  = function(a,b){
//    this.name = a;
//    this.method = b;
//};
//
//console.log(first);
//console.log(first.prototype);
//console.log(first.prototype.constructor);
//
//    var object1 = Object.create(first);
//
//
//console.log(object1);
//console.log(object1.prototype);
//console.log(object1.prototype.constructor);
//
//
//var object2 = new first();
//
//console.log(object2);
//console.log(object2.prototype);
var util = require("util");

var AbstractError = function (msg, constr) {
    Error.captureStackTrace(this, constr || this);
    this.message = msg || 'Error';
};

util.inherits(AbstractError, Error);
AbstractError.prototype.name = 'Abstract Error';
AbstractError.prototype.code = 500;

var DatabaseError = function (msg) {
//    DatabaseError.super_.call(this, msg, this.constructor);
};
util.inherits(DatabaseError, AbstractError);
console.log(DatabaseError.prototype.constructor);
DatabaseError.prototype.name = 'Database Transaction';

var hello = new DatabaseError('yo');
throw hello;