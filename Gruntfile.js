
module.exports = function(grunt) {
  grunt.initConfig({
    // The connect task is used to serve static files with a local server.
// The watch task is used to run tasks in response to file changes
jshint:{
  all:{
      options: {
        '-W061': true,
        '-W069': true,
        '-W099': true,
        '-W014': true
      },
      src:['backend/**/*.js']
    }
  },
watch: {

  client: {
    // '**' is used to include all subdirectories
    // and subdirectories of subdirectories, and so on, recursively.
    files: ['backend/**/*'],
    // In our case, we don't configure any additional tasks,
    // since livereload is built into the watch task,
    // and since the browser refresh is handled by the snippet.
    // Any other tasks to run (e.g. compile CoffeeScript) go here:

    tasks:['jshint:all'],
     options: {
      livereload: true,
      spawn:true

        // you can pass in any other options you'd like to the https server, as listed here: http://nodejs.org/api/tls.html#tls_tls_createserver_options_secureconnectionlistener
      }


  }

},
env : {
  options : {
      //Shared Options Hash
  },
    test : {
        NODE_ENV : 'test',
        DEST     : 'temp'
    },
  dev : {
      NODE_ENV : 'development',
      DEST     : 'temp'
  },
  prod : {
        NODE_ENV : 'production',
        DEST     : 'temp'
   },
  build : {
      NODE_ENV : 'production',
      DEST     : 'dist',
      extend   : {
          PATH     : {
              'value': 'node_modules/.bin',
              'delimiter': ':'
          }
      }
  }
},
foreverMulti: {
  basic: {
      file: 'wtf.js'
  }
},
      intern: {
          someReleaseTarget: {
              options: {
                  runType: 'client', // defaults to 'client'
                  config: 'tests/intern',
                  reporters: [ 'console']
//                  suites: [ 'myPackage/tests/all' ]
              }
          }
//          anotherReleaseTarget: { /* ... */ }
      },
shell: {
        server:{
            command: 'node wtf.js'
        },
        debug:{
          command: 'node --debug wtf.js'
        },
        dbrk:{
            command: 'node --debug-brk wtf.js'
        },
        mongo:{
            command: 'mongod'
        },
        cleanBuild: {                        // Target
            options: {                        // Options
                stdout: true
            },
            command: 'rm -rf js/lib'
        },                            // Task
        dojoBuild: {                        // Target
            options: {                        // Options
                stdout: true
            },
            command: 'js/util/buildscripts/build.sh --profile js/dojo-build.profile.js'
        }
    }
  });

    grunt.loadNpmTasks('grunt-env');
    grunt.loadNpmTasks('grunt-forever-multi');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks("grunt-contrib-watch");
    grunt.loadNpmTasks('grunt-shell');
    grunt.loadNpmTasks('intern');


    grunt.registerTask('unit-test', [ 'intern' ]);

grunt.registerTask('build', ['shell:cleanBuild','shell:dojoBuild']);
grunt.registerTask('test-server', ['env:test','shell:server']);
grunt.registerTask('dev-server', ['env:dev','shell:server']);

    grunt.registerTask('debug-server', ['env:test', 'shell:debug']);
    grunt.registerTask('debug-brk-server', ['env:dev', 'shell:dbrk']);

    grunt.registerTask('server', ['env:prod', 'shell:server']);
grunt.registerTask('preview', ['watch:client']);

// var changedFiles = Object.create(null);
// var onChange = grunt.util._.debounce(function() {
//   grunt.config.set('jshint.all.src', Object.keys(changedFiles));
//   changedFiles = Object.create(null);
// }, 200);

// grunt.event.on('watch', function(action, filepath, target) {
//   //change the source and destination in the uglify task at run time so that it affects the changed file only
//   // grunt.config('jshint.all.src', filepath);
//   changedFiles[filepath] = action;
//   onChange();

// });

};